import numpy as np


__all__ = ['Cat', ]


class Cat:
    """
    ### Sumary
        A schrodinger boolean
    
    ### Args:
        p_alive (float): Probability of the cat be alive

    """
    def __init__(self, p_alive = 0.5) -> None:
        self._p_alive = p_alive
        self._state = None
        v = np.array()
    
    def __repr__(self) -> str:
        return f"Cat({'Alive/Dead' if self._state is None else 'Alive' if self._state else 'Dead'})"
    
    def __str__(self) -> str:
        self.meansure()
        return self.__repr__()
    
    def __bool__(self) -> bool:
        return self.meansure()

    def meansure(self) -> bool:
        """
        ### Sumary
            Return the cat boolean value.
            The quantum state will colapse after it's measurement
        
        ### Returns
            boolean - True if the cat is alive, False otherwise
        """
        if self._state is None:
            self._state = self._p_alive > np.random.rand()
        return self._state
